#language: pt


Funcionalidade: Cadastrar um usuário
    Eu quero criar um cadastro na inmetrics
    Afim de utilizar as funcionalidades do site.

Cenário: Cadasto de usuário
    Dado o site da inmetrics
    E clicar sobre a opção cadastrar-se
    Quando preencher campo de usuário
    E preencher campo de senha
    E preecnher campo confirme senha
    E pressionar botão Cadastrar
    Então cria conta para usuário